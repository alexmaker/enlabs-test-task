package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
)

func TestInputSourceType(t *testing.T) {
	w := httptest.NewRecorder()
	id := uuid.New()

	body, err := json.Marshal(gin.H{"state": "win", "amount": "-10", "transactionId": id.String()})
	if err != nil {
		fmt.Println(body)
	}

	req, _ := http.NewRequest("POST", "/transactions", bytes.NewBuffer(body))
	req.Header.Add("Source-type", "incorrect")
	req.Header.Add("Content-Length", strconv.Itoa(len(body)))

	Server.Engine.ServeHTTP(w, req)

	p, err := ioutil.ReadAll(w.Body)
	if err != nil || strings.Index(string(p), `"code":202`) < 0 {
		t.Fail()
	}
}

func BenchmarkAddTransaction(b *testing.B) {
	App = new(Application)
	App.Init("testing")
	defer App.Db.Close()

	defer func() {
		App.Db.Exec("TRUNCATE TABLE transactions")
	}()

	w := httptest.NewRecorder()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		b.StopTimer()
		id := uuid.New()
		body, _ := json.Marshal(gin.H{"state": "win", "amount": "1", "transactionId": id.String()})
		req, _ := http.NewRequest("POST", "/transactions", bytes.NewBuffer(body))
		req.Header.Add("Source-type", "server")
		req.Header.Add("Content-Length", strconv.Itoa(len(body)))

		b.StartTimer()
		Server.Engine.ServeHTTP(w, req)

		if w.Code <= 200 || w.Code >= 300 {
			b.Error("Error code returns")
		}
	}
}
