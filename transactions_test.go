package main

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewTransaction(t *testing.T) {
	tr := NewTransaction("payment", State("lost"), 1)
	assert.Equal(t, tr.Source, "payment")
	assert.Equal(t, tr.State, State("lost"))
	assert.Equal(t, tr.IncomeAmount, Amount(1))

}

func TestTransaction_InputBodyPositiveLost(t *testing.T) {
	id := uuid.New().String()
	body := buildBody(`{"state": "lost", "amount": "10", "transactionId": "%s"}`, id)
	tr := new(Transaction)

	_ = json.Unmarshal(body, tr)
	assert.Equal(t, tr.State, State("lost"))
	assert.Equal(t, tr.IncomeAmount, Amount(10))
	assert.Equal(t, tr.Guid, id)

	if err := tr.IsValid(); err != nil {
		t.Error(err)
	}
	if err := tr.Prepare(); err != nil {
		t.Error(err)
	}

	assert.Equal(t, tr.Amount, float64(-10))
}

func TestTransaction_InputBodyNegativeAmount(t *testing.T) {
	id := uuid.New().String()
	body := buildBody(`{"state": "lost", "amount": "-10", "transactionId": "%s"}`, id)
	tr := new(Transaction)
	_ = json.Unmarshal(body, tr)

	if err := tr.IsValid(); err != nil {
		assert.Equal(t, tr.IncomeAmount, Amount(-10))
		return
	}

	t.Error("Negative Amount Test Failed")
}

func TestTransaction_InputBodyUnexpectedState(t *testing.T) {
	id := uuid.New().String()
	body := buildBody(`{"state": "test", "amount": "10", "transactionId": "%s"}`, id)
	tr := new(Transaction)
	_ = json.Unmarshal(body, tr)

	if err := tr.IsValid(); err != nil {
		assert.Equal(t, tr.State, State("test"))
		return
	}

	t.Error("Unexpected State Test Failed")
}

func TestTransaction_InputBodyTransactionIdRequired(t *testing.T) {
	body := buildBody(`{"state": "win", "amount": "10", "transactionId": ""}`)
	tr := new(Transaction)
	_ = json.Unmarshal(body, tr)

	if err := tr.IsValid(); err != nil {
		assert.Equal(t, tr.Guid, "")
		return
	}

	t.Error("Transactions Id Required Test Failed")
}

func TestTransaction_Save(t *testing.T) {
	tr := new(Transaction)
	getTr := new(Transaction)
	time := time.Now()

	tr.Guid = uuid.New().String()
	tr.State = State("win")
	tr.IncomeAmount = Amount(10)
	tr.Amount = float64(10)
	tr.Source = "payment"
	tr.Created = time

	if err := tr.Save(Acc); err != nil {
		t.Error(err)
	}

	assert.NotEqual(t, tr.Id, 0)

	defer func() {
		App.Db.Exec(`DELETE FROM transactions WHERE id = $1`, tr.Id)
	}()

	err := App.Db.QueryRowx(`SELECT * FROM transactions WHERE id = $1 LIMIT 1`, tr.Id).StructScan(getTr)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, tr.Id, getTr.Id)
	assert.Equal(t, tr.Guid, getTr.Guid)
	assert.Equal(t, tr.State, getTr.State)
	assert.Equal(t, tr.IncomeAmount, getTr.IncomeAmount)
	assert.Equal(t, tr.Amount, getTr.Amount)
	assert.Equal(t, tr.Source, getTr.Source)
	assert.Equal(t, getTr.Active, 1)
	assert.NotEmpty(t, getTr.Created)
}

func TestTransaction_CheckUniq(t *testing.T) {
	tr := new(Transaction)
	time := time.Now()

	tr.Guid = uuid.New().String()
	tr.State = State("win")
	tr.IncomeAmount = Amount(10)
	tr.Amount = float64(10)
	tr.Source = "payment"
	tr.Created = time

	if err := tr.Save(Acc); err != nil {
		t.Error(err)
	}

	assert.NotEqual(t, tr.Id, 0)

	defer func() {
		App.Db.Exec(`DELETE FROM transactions WHERE id = $1`, tr.Id)
	}()

	if err := tr.CheckUniq(); err != nil {
		return
	}

	t.Error("Transaction CheckUniq Test Failed")
}
