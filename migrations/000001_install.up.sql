CREATE TABLE IF NOT EXISTS accounts (
   id serial PRIMARY KEY,
   balance NUMERIC(18, 2) NOT NULL
);

CREATE TABLE IF NOT EXISTS transactions (
   id serial PRIMARY KEY,
   guid VARCHAR(300) NOT NULL,
   account_id INT NOT NULL,
   amount NUMERIC(18, 2) NOT NULL,
   income_amount NUMERIC(18, 2) NOT NULL,
   state VARCHAR (10) NOT NULL,
   source VARCHAR(50) NOT NULL,
   active INT DEFAULT 1 NOT NULL,
   created TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
   FOREIGN KEY (account_id) REFERENCES accounts (id),
   UNIQUE (guid)
);

INSERT INTO accounts VALUES(1, 0);