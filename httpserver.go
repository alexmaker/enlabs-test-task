package main

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

type HttpServer struct {
	Engine *gin.Engine
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

func NewHttpServer() *HttpServer {
	s := new(HttpServer);
	s.Engine = gin.New();

	return s
}

func (s *HttpServer) SetupMiddlewares() {
	s.Engine.Use(gin.Recovery())
	s.Engine.Use(s.LoggerMiddleware())
	s.Engine.Use(s.SourceTypeValidateMiddleware())
}

func (s *HttpServer) SetupRouters() {
	s.Engine.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	s.Engine.GET("/account", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "account",
		})
	})
	s.Engine.POST("/transactions", s.addTransactionRouter)
}

func (s *HttpServer) Run() {
	if err := Server.Engine.Run(); err != nil {
		App.ErrorFatal(err)
	}
}

/* Routers */
func (s *HttpServer) addTransactionRouter(c *gin.Context) { // Uri: POST /transactions
	var t *Transaction

	if err := c.ShouldBindJSON(&t); err != nil {
		c.JSON(http.StatusBadRequest, s.Error(100, err))
		return
	}
	if err := c.ShouldBindHeader(&t); err != nil {
		c.JSON(http.StatusBadRequest, s.Error(101, err))
		return
	}
	if err := t.IsValid(); err != nil {
		c.JSON(http.StatusBadRequest, s.Error(200, err))
		return
	}
	if err := App.Db.Ping(); err != nil {
		c.JSON(http.StatusServiceUnavailable, s.Error(600, err))
		return
	}
	if err := t.CheckUniq(); err != nil { // Check Idempotent key
		c.JSON(http.StatusFound, s.Error(201, err))
		return
	}
	if err := Acc.Operation(t); err != nil { // Register the transaction
		c.JSON(http.StatusBadRequest, s.Error(202, err))
		return
	}

	/* Return data If Ok */
	data := gin.H{"transaction": t, "account": Acc}
	c.JSON(http.StatusCreated, s.Success(data))
}

func (s *HttpServer) Error(code int, err error) gin.H {
	return gin.H{"error": gin.H{"code": code, "msg": err.Error()}}
}

func (s *HttpServer) Success(data gin.H) gin.H {
	return gin.H{"result": "success", "data": data}
}

/* Middlewares */
func (s *HttpServer) SourceTypeValidateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		source := c.Request.Header.Get("Source-type")
		sl := App.Config.GetStringSlice("sources")

		for _, v := range sl {
			if v == source {
				c.Next()
				return
			}
		}

		c.AbortWithStatusJSON(http.StatusBadRequest, s.Error(202,
			fmt.Errorf("Incorrect source type. Expect %s", sl)))
	}
}

func (s *HttpServer) LoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		var buf bytes.Buffer
		tee := io.TeeReader(c.Request.Body, &buf)
		body, _ := ioutil.ReadAll(tee)
		bodyString := strings.Trim(string(body), "\t \n")
		c.Request.Body = ioutil.NopCloser(&buf)

		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery
		blw := &bodyLogWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = blw
		c.Next() // Process request

		if raw != "" {
			path = path + "?" + raw
		}

		status := c.Writer.Status()
		var response string
		if status != 200 && status != 201 {
			response = blw.body.String()
		}

		App.Logf("%s %s %s Source-type: %s %s | %d | %s",
			c.ClientIP(),
			c.Request.Method,
			path,
			c.GetHeader("Source-type"),
			bodyString,
			status,
			response)
	}
}
