package main

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestGetAccount(t *testing.T) {
	acc, err := GetAccount(1)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, acc.Id, 1)
}

func TestAccount_CheckBalance(t *testing.T) {
	val := -1 * (Acc.GetBalance() + 1)
	if err := Acc.CheckBalance(val); err != nil {
		return
	}

	t.Error("Account CheckBalance Test Failed")
}

func TestAccount_Operation(t *testing.T) {
	tr := new(Transaction)
	tr.Guid = uuid.New().String()
	tr.State = State("win")
	tr.IncomeAmount = Amount(10)
	tr.Amount = float64(10)
	tr.Source = "payment"
	tr.Created = time.Now()

	balance := Acc.GetBalance()
	if err := Acc.Operation(tr); err != nil {
		t.Error(err)
	}

	assert.NotEqual(t, balance, Acc.Balance)
	assert.Equal(t, balance+10, Acc.Balance)
	App.Db.Exec(`DELETE FROM transactions WHERE id = $1`, tr.Id)

	if err := Acc.RecalculateBalance(); err != nil {
		t.Error("Account Recalculate Balance Failed")
	}
	assert.Equal(t, balance, Acc.Balance)
}

func TestAccount_CancelTransactions(t *testing.T) {
	defer func() {
		App.Db.Exec("TRUNCATE TABLE transactions")
	}()

	amount := float64(10)
	Acc.RecalculateBalance()
	balance := Acc.GetBalance()

	for i := 0; i < 10; i++ {
		tr := new(Transaction)
		tr.State = State("win")
		tr.IncomeAmount = Amount(amount)
		tr.Amount = float64(amount)
		tr.Source = "payment"
		tr.Guid = uuid.New().String()
		tr.Created = time.Now()
		Acc.Operation(tr)
	}
	assert.Equal(t, balance+amount*10, Acc.GetBalance())

	balance = Acc.GetBalance()
	count := Acc.CancelTransactions()
	assert.Equal(t, int64(5), count)
	assert.Equal(t, balance-amount*float64(count), Acc.GetBalance())

	balance = Acc.GetBalance()
	count = Acc.CancelTransactions()
	assert.Equal(t, int64(3), count)
	assert.Equal(t, balance-amount*float64(count), Acc.GetBalance())

	balance = Acc.GetBalance()
	count = Acc.CancelTransactions()
	assert.Equal(t, int64(1), count)
	assert.Equal(t, balance-amount*float64(count), Acc.GetBalance())

	balance = Acc.GetBalance()
	count = Acc.CancelTransactions()
	assert.Equal(t, int64(1), count)
	assert.Equal(t, float64(0), Acc.GetBalance())
}
