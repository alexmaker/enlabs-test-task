package main

import (
	"database/sql"
	"errors"
	"math"
	"time"
)

type Account struct {
	Id      int     `json:"accountId" db:"id"`
	Balance float64 `json:"balance" db:"balance"` // Cache Value
}

func (a *Account) Operation(t *Transaction) error {
	t.Created = time.Now()

	if err := t.Prepare(); err != nil {
		return err
	}
	if err := a.CheckBalance(t.Amount); err != nil {
		return err
	}
	if err := t.Save(a); err != nil {
		return err
	}
	if err := a.ChangeBalance(t.Amount); err != nil {
		return err
	}

	return nil
}

func (a *Account) CheckBalance(f float64) error {
	balance := a.GetBalance()

	if balance+f < 0 {
		return errors.New("Have no money to execute the transaction ")
	}

	return nil
}

func (a *Account) GetBalance() float64 {
	return math.Round(a.Balance*100) / 100
}

func (a *Account) ChangeBalance(amount float64) error {
	balance := a.Balance + amount

	tx, err := App.Db.Begin()
	if err != nil {
		return err
	}

	_, err = tx.Exec("UPDATE accounts SET balance = $1 WHERE id = 1", balance)
	if err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		tx.Rollback();
		return err
	}

	a.Balance = balance

	return nil
}

func (a *Account) RecalculateBalance() error {
	err := App.Db.Get(&a.Balance,
		`UPDATE accounts SET balance = (SELECT COALESCE(SUM(amount), 0)
			   FROM transactions WHERE account_id = $1 AND active = 1)
			   WHERE Id = $1 RETURNING balance`, a.Id)
	if err != nil {
		return err
	}

	return nil
}

func (a *Account) CancelTransactionsByTimer(timeout time.Duration) {
	if timeout == 0 {
		return
	}

	App.Logf("CancelTransactionsByTimer() starts every %s", timeout)
	for _ = range time.Tick(timeout) {
		a.CancelTransactions()
	}
}

func (a *Account) CancelTransactions() int64 {
	errPrefix := "CancelTransactions Error"

	query := `UPDATE transactions SET active = 0 WHERE id IN (
				SELECT id FROM (
					SELECT id, row_number() over(order by created, id DESC) as pos
					FROM transactions 
					WHERE active = 1 AND account_id = $1
					ORDER BY created, id DESC
				) as res WHERE MOD(pos, 2) = 1
				LIMIT 10 
			)`

	App.Log("CancelTransactions is working now")
	tx, err := App.Db.Begin()
	if err != nil {
		App.Logf("%s: %s", errPrefix, err)
	}

	var result sql.Result
	result, err = tx.Exec(query, a.Id)
	if err != nil {
		App.Logf("%s: %s", errPrefix, err)
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback();
		App.Logf("%s: %s", errPrefix, err)
	}

	rows, _ := result.RowsAffected()
	App.Logf("Balance is %.2f. %d transactions are canceled", a.GetBalance(), rows)
	if rows > 0 {
		if err = a.RecalculateBalance(); err != nil {
			App.Logf("%s: %s", errPrefix, err)
		}
		App.Logf("Done. Balance is %.2f now", a.GetBalance())
	}

	return rows
}

func GetAccount(Id int) (*Account, error) {
	acc := new(Account)

	err := App.Db.QueryRowx("SELECT id, balance FROM accounts WHERE id = $1 LIMIT 1", Id).StructScan(acc)
	if err != nil {
		return nil, err
	}

	return acc, nil
}
