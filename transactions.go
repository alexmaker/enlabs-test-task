package main

import (
	"errors"
	"github.com/google/uuid"
	"time"
)

type Transaction struct {
	Id           int       `json:"-" db:"id"`
	AccountId    int       `json:"-" db:"account_id"`
	IncomeAmount Amount    `json:"amount,string" binding:"required" db:"income_amount"`
	Amount       float64   `json:"-" db:"amount"`
	Guid         string    `json:"transactionId" binding:"required" db:"guid"`
	State        State     `json:"state" binding:"required" db:"state"`
	Source       string    `json:"-" header:"Source-Type" db:"source"`
	Active       int     `json:"-" db:"active"`
	Created      time.Time `json:"-" db:"created"`
}

type State string
type Amount float64

const (
	Win  State = "win"
	Lost State = "lost"
)

func (s State) IsValid() error {
	switch s {
	case Win, Lost:
		return nil
	}
	return errors.New("Invalid state type. Expect win or lost. ")
}

func (a Amount) IsValid() error {
	if a > 0 {
		return nil
	}

	return errors.New("Amount must be bigger than 0. ")
}

func NewTransaction(source string, state State, amount Amount) *Transaction {
	t := &Transaction{Source: source, State: state, IncomeAmount: amount}
	t.Guid = uuid.New().String()

	return t
}

func (t *Transaction) IsValid() error {
	if err := t.IncomeAmount.IsValid(); err != nil {
		return err
	}
	if err := t.State.IsValid(); err != nil {
		return err
	}
	if len(t.Guid) == 0 {
		return errors.New("transactionId is required")
	}

	return nil
}

func (t *Transaction) CheckUniq() error {
	var Id int

	stmt, err := App.Db.Preparex(`SELECT id FROM transactions WHERE guid = $1`)
	if err != nil {
		App.Log(err)
	}

	_ = stmt.Get(&Id, t.Guid)
	if Id > 0 {
		return errors.New("Transaction already exists ")
	}

	return nil
}

func (t *Transaction) Prepare() error {
	if t.State == Lost {
		t.Amount = float64(-1 * t.IncomeAmount)
		return nil
	}

	t.Amount = float64(t.IncomeAmount)
	return nil
}

func (t *Transaction) Save(a *Account) error {
	t.AccountId = a.Id
	if t.Created.IsZero() {
		t.Created = time.Now()
	}

	query := `INSERT INTO transactions (guid, account_id, amount, income_amount, state, source, created) 
				VALUES (:guid, :account_id, :amount, :income_amount, :state, :source, :created) RETURNING Id`
	stmt, err := App.Db.PrepareNamed(query)
	if err != nil {
		return err
	}

	var Id int
	if err := stmt.Get(&Id, t); err != nil {
		return err
	}

	t.Id = Id

	return nil
}
