package main

import (
	"fmt"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"log"
	"os"
)

const (
	ConfigName   = "config"
	ConfigType   = "yaml"
	ConfigPath   = "."
	LoggerPrefix = "[APP]"
	AccountId    = 1
)

type Application struct {
	Db     *sqlx.DB
	Config *viper.Viper
	Logger *log.Logger
}

var (
	App    *Application
	Server *HttpServer
	Acc    *Account
)

func main() {
	var err error
	App = NewApplication()
	App.Init("production")
	defer App.Db.Close()

	f := App.LoggerInit()
	defer f.Close()

	App.MigrationUp()
	Acc, err = GetAccount(AccountId) // Use AccountId = 1 by default
	if err != nil {
		App.ErrorFatal(err)
	}


	err = Acc.RecalculateBalance() // Recalculate If main starts
	if err != nil {
		log.Fatalln(err)
	}
	// Run the cleaner
	go Acc.CancelTransactionsByTimer(App.Config.GetDuration("timer"))
	// Run the http server
	Server = NewHttpServer()
	Server.SetupMiddlewares()
	Server.SetupRouters()
	Server.Run()
}

func NewApplication() *Application {
	return &Application{}
}

func (a *Application) Init(dbtype string) {
	if err := a.PrepareConfig(); err != nil {
		a.ErrorFatal(err)
	}
	if err := a.PrepareDatabase(dbtype); err != nil {
		a.ErrorFatal(err)
	}
	if err := a.Db.Ping(); err != nil {
		a.ErrorFatal(err)
	}
}

func (a *Application) MigrationUp() {
	driver, err := postgres.WithInstance(App.Db.DB, &postgres.Config{})
	if err != nil {
		App.ErrorFatal(err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://migrations",
		"postgres", driver)

	if err != nil {
		App.ErrorFatal(err)
	}

	m.Up();
}

func (a *Application) PrepareConfig() error {
	a.Config = viper.GetViper()
	a.Config.SetConfigName(ConfigName)
	a.Config.SetConfigType(ConfigType)
	a.Config.AddConfigPath(ConfigPath)
	if err := a.Config.ReadInConfig(); err != nil {
		return fmt.Errorf("PrepareConfig Fatal Error: %s", err)
	}

	return nil
}

func (a *Application) PrepareDatabase(dbtype string) error {
	var err error

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=%s",
		a.Config.Get("db.host"),
		a.Config.Get("db.username"),
		a.Config.Get("db.password"),
		a.Config.Get(fmt.Sprintf("db.%s", dbtype)),
		a.Config.Get("db.sslmode"))

	a.Db, err = sqlx.Open("postgres", dsn)
	if err != nil {
		return fmt.Errorf("PrepareDatabase Fatal Error: %s", err)
	}

	return nil
}

func (a *Application) LoggerInit() *os.File {
	fn := a.Config.GetString("log.file")

	log.SetPrefix(fmt.Sprintf("%s ", LoggerPrefix))
	f, err := os.OpenFile(fn, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		a.ErrorNotice(err)
	}
	log.SetOutput(f)

	return f
}

func (a *Application) ErrorFatal(err error) {
	log.Fatalln(fmt.Sprintf("Fatal Error: %s", err))
}

func (a *Application) ErrorNotice(err error) {
	log.Println(fmt.Sprintf("Notice: %s", err))
}

func (a *Application) Log(v interface{}) {
	log.Println(v)
}

func (a *Application) Logf(f string, args ...interface{}) {
	log.Println(fmt.Sprintf(f, args...))
}
