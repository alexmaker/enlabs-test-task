FROM golang:latest

RUN apt-get update \
    && apt-get install -y curl
RUN curl -L https://github.com/golang-migrate/migrate/releases/download/v4.10.0/migrate.linux-amd64.tar.gz | tar xvz \
    && mv migrate.linux-amd64 /bin/migrate
RUN migrate -version

RUN mkdir -p /go/src/app
WORKDIR /go/src/app
ADD . /go/src/app
RUN go get -v
RUN go get -t
CMD go test && go build -o app . && ./app
