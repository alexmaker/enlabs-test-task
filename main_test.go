package main

import (
	"fmt"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	_ "github.com/lib/pq"
	"os"
	"testing"
	//"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	var err error

	App = new(Application)
	App.Init("testing")
	defer App.Db.Close()

	// Migrations
	driver, err := postgres.WithInstance(App.Db.DB, &postgres.Config{})
	mi, err := migrate.NewWithDatabaseInstance(
		"file://migrations",
		"postgres", driver)

	if err := mi.Up(); err != nil {
		App.ErrorFatal(err)
	}

	Acc, err = GetAccount(AccountId) // Use AccountId = 1 by default
	if err != nil {
		App.ErrorFatal(err)
	}

	Server = NewHttpServer()
	Server.SetupMiddlewares()
	Server.SetupRouters()
	code := m.Run()
	mi.Down()
	os.Exit(code)
}

/* Helpers */
func buildBody(f string, args ...interface{}) []byte {
	return []byte(fmt.Sprintf(f, args...))
}
