## Quick start
All you need... Git, Docker and Docker Compose.
```bash
git clone git@bitbucket.org:alexmaker/enlabs-test-task.git
cd enlabs-test-task
docker-compose up
```

If you want to run tests manually you follow below:
```bash
docker-compose exec app sh -c "go test"
docker-compose exec app sh -c "go test -bench=."
```

## Migrations
Migrations load when app loads to the easier testing. But app container contains golang-migrate/migrate CLI. Examples of usage migrate.

```bash
migrate -source file://migrations -database postgres://postgres-dev:12secret12@localhost:5432/app?sslmode=disable up
migrate -source file://migrations -database postgres://postgres-dev:12secret12@localhost:5432/app?sslmode=disable down
```

## JSON Errors Codes 
* 100 - Binding JSON Error
* 101 - Binding Headers Error
* 200 - Transaction Validation Error
* 201 - Transaction Already Exists
* 202 - Incorrect Source-type
* 300 - Save Transaction to Database Error
* 601 - Database Error 

## Notes
I wanted use Redis to storage Balance and saved TransactionIds (that explains Account.Balance architecture), but test task didn't contain Redis in stack.
